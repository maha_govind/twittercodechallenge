package com.twitter.challenge.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.twitter.challenge.networking.responses.WeatherResponse;
import com.twitter.challenge.networking.retrofit.RetrofitClient;
import com.twitter.challenge.networking.retrofit.WeatherService;
import com.twitter.challenge.util.WeatherUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherViewModel extends AndroidViewModel {
    private int numberOFDays = 1;
    private final int TOTAL_DAYS = 5;
    private WeatherService weatherService;
    private MutableLiveData<WeatherResponse> currentWeatherResponse = new WeatherResponse();
    private MutableLiveData<WeatherResponse> futureWeatherResponse = new WeatherResponse();
    private double stdDev = Double.MIN_VALUE;

    public WeatherViewModel(@NonNull Application application) {
        super(application);
    }

    public void getCurrentWeather() {
        weatherService = RetrofitClient.getService();
        Call<WeatherResponse> call = weatherService.getCurrentWeather();
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                Log.i("Current Weather Success", response.toString());
                if (response.body() != null) {
                    currentWeatherResponse.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                Log.i("Current Weather Failure", t.getLocalizedMessage());
            }
        });
    }

    public void getFutureWeather() {
        Call<WeatherResponse> call = weatherService.getFutureWeather(numberOFDays);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                Log.i("Future Weather Success " + numberOFDays, response.toString());
                if (response.body() != null) {
                    futureWeatherResponse.postValue(response.body());
                    if (numberOFDays != TOTAL_DAYS) {
                        numberOFDays++;
                        getFutureWeather();
                    }
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                Log.i("Future Weather Failure", t.getLocalizedMessage());
            }
        });
    }

    public MutableLiveData<WeatherResponse> getCurrentWeatherResponse() {
        return currentWeatherResponse;
    }

    public MutableLiveData<WeatherResponse> getFutureWeatherResponse() {
        return futureWeatherResponse;
    }

    public double getStdDev(List<Double> temperatureList) {
        stdDev = WeatherUtil.calculateStdDeviation(temperatureList);
        return stdDev;
    }

    public double getStdDev() {
        return stdDev;
    }
}
