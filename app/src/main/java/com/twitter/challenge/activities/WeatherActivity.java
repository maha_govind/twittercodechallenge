package com.twitter.challenge.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.twitter.challenge.R;
import com.twitter.challenge.networking.responses.WeatherResponse;
import com.twitter.challenge.util.WeatherUtil;
import com.twitter.challenge.viewmodel.WeatherViewModel;

import java.util.ArrayList;
import java.util.List;

public class WeatherActivity extends AppCompatActivity {
    private TextView temperatureView;
    private TextView windView;
    private TextView stdDevView;
    private TextView locationView;
    private ImageView cloudView;
    private Button calculateStdDevButton;
    private ProgressBar progressBar;

    double stdDev = Double.MIN_VALUE;
    private List<Double> temperatureList = new ArrayList<>();
    private final int TOTAL_DAYS = 5;

    private WeatherViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = ViewModelProviders.of(this).get(WeatherViewModel.class);
        observeViewModel();

        if (savedInstanceState == null) {
            viewModel.getCurrentWeather();
        }

        progressBar = findViewById(R.id.progressBar);
        locationView = findViewById(R.id.location);
        temperatureView = findViewById(R.id.temperature);
        windView = findViewById(R.id.wind_speed);
        stdDevView = findViewById(R.id.std_deviation);
        if (viewModel.getStdDev() != Double.MIN_VALUE) {
            stdDevView.setText(String.format("%.2f", viewModel.getStdDev()));
        }

        cloudView = findViewById(R.id.icon_cloudy);
        calculateStdDevButton = findViewById(R.id.calculate_std_deviation);
        calculateStdDevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewModel.getStdDev() == Double.MIN_VALUE) {
                    progressBar.setVisibility(View.VISIBLE);
                    viewModel.getFutureWeather();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void updateView(WeatherResponse response) {
        double temperature = response.getWeather().getTemp();
        temperatureView.setText(getString(R.string.temperature, String.format("%.2f", temperature), String.format("%.2f", WeatherUtil.celsiusToFahrenheit(temperature))));
        windView.setText(getString(R.string.wind_speed, String.format("%.2f", response.getWind().getSpeed())));
        cloudView.setVisibility(response.getClouds().getCloudiness() > 50 ? View.VISIBLE : View.GONE);
        locationView.setText(response.getName());
    }

    private void observeViewModel() {
        viewModel.getCurrentWeatherResponse().observe(this, new Observer<WeatherResponse>() {
            @Override
            public void onChanged(@Nullable WeatherResponse response) {
                if (response != null) {
                    updateView(response);
                }
            }
        });

        viewModel.getFutureWeatherResponse().observe(this, new Observer<WeatherResponse>() {
            @Override
            public void onChanged(@Nullable WeatherResponse response) {
                if (response != null) {
                    temperatureList.add(response.getWeather().getTemp());
                }
                if (temperatureList.size() == TOTAL_DAYS) {
                    progressBar.setVisibility(View.INVISIBLE);
                    stdDev = viewModel.getStdDev(temperatureList);
                    if (stdDev != Integer.MIN_VALUE) {
                        stdDevView.setText(String.format("%.2f", stdDev));
                    }
                }
            }
        });
    }
}
