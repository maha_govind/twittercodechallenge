package com.twitter.challenge.util;

import java.util.List;

public class WeatherUtil {
    /**
     * Converts temperature in Celsius to temperature in Fahrenheit.
     *
     * @param temperatureInCelsius Temperature in Celsius to convert.
     * @return Temperature in Fahrenheit.
     */
    public static double celsiusToFahrenheit(double temperatureInCelsius) {
        return temperatureInCelsius * 1.8f + 32;
    }

    /**
     * Calculates Std Deviation
     *
     * @param temperatureList List of temperature values.
     * @return std dev
     */
    public static double calculateStdDeviation(List<Double> temperatureList) {
        if (temperatureList == null || temperatureList.size() < 2) {
            return Integer.MIN_VALUE;
        }
        int numberOfDays = temperatureList.size();
        double mean = calculateMean(temperatureList, numberOfDays);
        double variance = 0;
        for (double temp : temperatureList) {
            variance += Math.pow((temp - mean), 2);
        }
        variance /= numberOfDays - 1;
        return Math.sqrt(variance);
    }

    /**
     * Calculates Mean
     *
     * @param temperatureList List of temperature values.
     * @param numberOfDays    number of days.
     * @return mean
     */
    private static double calculateMean(List<Double> temperatureList, int numberOfDays) {
        double sum = 0;
        for (double temperature : temperatureList) {
            sum += temperature;
        }
        return sum / numberOfDays;
    }
}
