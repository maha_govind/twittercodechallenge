package com.twitter.challenge.networking.retrofit;

import com.twitter.challenge.networking.responses.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface WeatherService {

    @GET("current.json")
    Call<WeatherResponse> getCurrentWeather();

    @GET("future_{number_of_days}.json")
    Call<WeatherResponse> getFutureWeather(@Path("number_of_days") int numberOfDays);
}
