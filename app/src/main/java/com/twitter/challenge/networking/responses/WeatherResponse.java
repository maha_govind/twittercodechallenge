
package com.twitter.challenge.networking.responses;

import android.arch.lifecycle.MutableLiveData;

import com.google.gson.annotations.SerializedName;
import com.twitter.challenge.models.Clouds;
import com.twitter.challenge.models.Weather;
import com.twitter.challenge.models.Wind;

public class WeatherResponse extends MutableLiveData {

    @SerializedName("weather")
    private Weather weather;
    @SerializedName("wind")
    private Wind wind;
    @SerializedName("clouds")
    private Clouds clouds;
    @SerializedName("name")
    private String name;

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
