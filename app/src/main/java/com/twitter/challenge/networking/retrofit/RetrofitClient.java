package com.twitter.challenge.networking.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit = null;
    private static final String BASE_URL="http://twitter-code-challenge.s3-website-us-east-1.amazonaws.com/";

    private static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static WeatherService getService() {
        retrofit = getClient();
        return retrofit.create(WeatherService.class);
    }
}