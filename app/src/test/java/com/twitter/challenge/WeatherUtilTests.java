package com.twitter.challenge;

import com.twitter.challenge.util.WeatherUtil;

import org.assertj.core.data.Offset;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.within;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class WeatherUtilTests {
    @Test
    public void testCelsiusToFahrenheitConversion() {
        final Offset<Double> precision = within(0.01);

        assertThat(WeatherUtil.celsiusToFahrenheit(-50)).isEqualTo(-58, precision);
        assertThat(WeatherUtil.celsiusToFahrenheit(0)).isEqualTo(32, precision);
        assertThat(WeatherUtil.celsiusToFahrenheit(10)).isEqualTo(50, precision);
        assertThat(WeatherUtil.celsiusToFahrenheit(21.11f)).isEqualTo(70, precision);
        assertThat(WeatherUtil.celsiusToFahrenheit(37.78f)).isEqualTo(100, precision);
        assertThat(WeatherUtil.celsiusToFahrenheit(100)).isEqualTo(212, precision);
        assertThat(WeatherUtil.celsiusToFahrenheit(1000)).isEqualTo(1832, precision);
    }

    @Test
    public void calculateStdDeviation() {
        final Offset<Double> precision = within(0.01);

        List<Double> list = new ArrayList<>(Arrays.asList(16.83, 11.15, 14.2, 9.88, 19.19));
        assertThat(WeatherUtil.calculateStdDeviation(list)).isEqualTo(3.8655335983535, precision);
        list = new ArrayList<>(Arrays.asList(16.83, 11.15, 14.2, 9.88, 19.19, -23.5, -12.98));
        assertThat(WeatherUtil.calculateStdDeviation(list)).isEqualTo(16.447414907603
                , precision);
        list = new ArrayList<>();
        assertThat(WeatherUtil.calculateStdDeviation(list)).isEqualTo(Integer.MIN_VALUE, precision);
    }
}
