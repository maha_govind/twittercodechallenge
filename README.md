# README #
Twitter Coding challenge

# Features #
● Displays the following current conditions:
a. Temperature in Celsius and Fahrenheit
b. Wind speed.
c. A cloud icon if the cloudiness percentage is greater than 50%.

● Button to calculate Std deviation for next 5 days 

￼￼￼● Handles configuration and orientation changes without hitting the
network again - using ViewModel and LiveData

● Shows a progress spinner when network speed is slow.

● Third-party libraries - retrofit for networking 

● Unit test to test the std deviation functionality
